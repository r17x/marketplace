from django.contrib import admin
from django.forms import ModelForm
from organic.models import Product

class ProductAdmin(admin.ModelAdmin):
    fieldExclude = ["id", "date_modified"]
    list_display = [field.name for field in Product._meta.fields if field.name not in fieldExclude]
    search_fields = ('name',)
    ordering = ('-id',)
    
    
admin.site.register(Product, ProductAdmin)