# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('description', models.TextField(help_text=b'Deskripsi produk', null=True, verbose_name=b'deskripsi', blank=True)),
                ('photo', models.ImageField(upload_to=b'photoproduct', null=True, verbose_name=b'Photo', blank=True)),
                ('active', models.BooleanField(default=True)),
                ('price', models.DecimalField(null=True, max_digits=15, decimal_places=2, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
