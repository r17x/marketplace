# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organic', '0002_cart_item'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cart',
            name='unique_id',
        ),
        migrations.AddField(
            model_name='cart',
            name='user',
            field=models.ForeignKey(default='1', verbose_name=b' user', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
